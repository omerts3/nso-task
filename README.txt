Hello there!

To run the project,
1) Make sure port 3000 is available, or change it in app.js > CONSTANTS.
2) CD to the folder and run "npm install".
3) After installation finished, run "node app.js".
4) Browse to "localhost:3000", have client and server consoles open and watch while the magic happens :)

Tal Yael.