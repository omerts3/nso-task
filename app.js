const express = require("express");
const bodyParser = require("body-parser");

// Configuring constants
const CONSTANTS = {
	port: 3000
};

// Creating app, initializing middleware
const app = express();
app.use(bodyParser.json());
app.use('/libs', express.static(__dirname + '/libs'));
app.use('/views', express.static(__dirname + '/views'));

// Routes
app.all('/', function(req, res) {
	res.sendFile("views/index.html", { root: __dirname });
});
app.post('/log', function(req, res) {
	let date = new Date(req.body.timestamp); // Possibly a date formatter for aesthetics.
	console.log(date.toLocaleString(), '|', req.body.method, '|', req.body.message);
	res.send();
});

// Start app
app.listen(CONSTANTS.port, function() {
	console.log("App is listening on port " + CONSTANTS.port + ".");
});