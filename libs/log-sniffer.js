(function(c) {
	// Lib Constants
	const CONSTANTS = {
		logPostUrl: '/log',
		logMethods: ['log', 'warn', 'error', 'info', 'debug']
	}
	
	// This function will build and send the request object.
	function postLogToServer(method, message, timestamp) {
		let logPost = new XMLHttpRequest();
		logPost.open('POST', CONSTANTS.logPostUrl, true);
		logPost.setRequestHeader("Content-type", "application/json");
		// Since this is a one-way message, response handler will be empty.
		logPost.onreadystatechange = function() {};
		let log = {
			method: method,
			message: message,
			timestamp: timestamp
		}
		logPost.send(JSON.stringify(log));
	}
	
	/* Running the actual library code. 
		Will go through all specified methods and wrap original console with a function. */
	CONSTANTS.logMethods.forEach(method => {
		let nativeFunc = c[method];
		c[method] = function(text) {
			// 'timestamp' is created when function is called!
			let timestamp = Date.now();
			postLogToServer(method, text, timestamp);
			nativeFunc.apply(c, arguments);
		}
	})
	
})(window.console)